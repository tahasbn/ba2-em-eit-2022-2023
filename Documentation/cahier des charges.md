# Fast and Furious - Conception et réalisation d’un robot de course capable de sauter un tremplin

Le but du projet est de créer, en équipe, un robot capable d’effectuer un trajet en S, de sauter grâce à un tremplin, de se réceptionner et d’effectuer un deuxième S afin d’atteindre l’arrivée le plus rapidement possible (voir ci-dessous).

![Plateau de départ](img/plateau1.png)

![Plateau d'arrivée](img/plateau2.png)

Le véhicule devra être autonome.  Il pourra s’aider du marquage noir afin de se guider.  Les étudiants devront sélectionner les différents composants du véhicule (moteurs, capteurs, batterie, microcontrôleur, …).  Ils auront accès au Fablab de l’ULB afin de le construire grâce aux imprimantes 3D et découpeuses laser.  Une modélisation sera réalisée afin d’optimiser le déplacement du robot et de déterminer la longueur du saut.

## Déroulement du projet

### Premier quadrimestre : Modélisation et réalisation d’un prototype

Dans un premier temps, les étudiants devront réaliser une étude bibliographique en s'aidant de livres de référence, revues, ressources Internet...

Ils concevront ensuite un robot qui respecte le cahier des charges.  La réalisation finale devra être une conception originale et pas une copie d'un robot trouvé sur Internet ou dans les livres de référence.

Les étudiants devront

- Établir un plan d'actions et un planning dès la semaine 3 et les affiner tout au long du projet
- Étudier et comparer différentes solutions trouvées aux problèmes posés, pour pouvoir justifier le choix de la solution retenue
- Sélectionner et justifier le choix des différents éléments de leur robot (moteurs, capteurs, batterie, microcontrôleur…)
- Modéliser et simuler le déplacement du robot.  Ils utiliseront ensuite ce simulateur pour dimensionner la régulation des moteurs et le cas échéant l'odométrie
- Modéliser la longueur du saut
- Concevoir les pièces nécessaires dans un logiciel de CAO (Fusion 360, Solidworks...).

Dans le courant du mois de novembre, les étudiants présenteront leur état d’avancement, notamment la partie modélisation - régulation.

A la fin du premier quadrimestre, les étudiants devront présenter leur(s) prototype(s) et les algorithmes mis en œuvre.

### Second quadrimestre : Validation et optimisation du prototype

Durant le second quadrimestre, les étudiants testeront et amélioreront le prototype obtenu à la fin du premier quadrimestre.  Ils optimiseront les composants du robot afin de maximiser la vitesse du robot et la longueur du saut.  Ils devront affiner leur modèle afin de prédire le plus précisément possible la longueur du saut de leur robot.

## Évaluation

L'évaluation des groupes se fera comme stipulé dans le guide fourni aux étudiants en début d'année (rapports, présentations intermédiaire et finale, fonctionnement du groupe, …).
L’évaluation des prototypes se fera le vendredi 17 mars 2023 sur le temps de midi.

Chaque groupe fournira une version électronique de ses rapports (format pdf), des différentes présentations qui auront été utilisées et éventuellement des photos et vidéos de leur robot.

A la fin du projet, les étudiants rendront les prototypes qu’ils auront développés.

## Personnes-ressources

Afin d'aider les étudiants dans leur tâche, différentes personnes spécialisées dans les domaines abordés dans ce projet peuvent être consultées pour obtenir des informations.  Toutefois, ces consultations devront se faire ponctuellement, sur rendez-vous et/ou suivant un horaire établi avec la personne en question (contact uniquement par e-mail).  Les étudiants devront venir avec des questions concrètes et réfléchies.

- Conception mécanique: Christophe Reyntiens (christophe.reyntiens@ulb.be), Michel Osée (michel.osee@ulb.be)
- Contrôle électronique : Christophe Reyntiens (christophe.reyntiens@ulb.be), Michel Osée (michel.osee@ulb.be)
- Régulation : Laurent Catoire (Laurent.Catoire@ulb.be)
