# caracMoteur

Code permettant de mesurer la vitesse maximum d'un moteur.  Il est basé sur *testmoteur* et *testEncodeur*.

Le comptage des impulsions de l'encodeur est fait par une *routine d'interruption* comme pour *testEncodeur*.

Ce code utilise une fonction pour contrôler le moteur.  Cela a plusieurs avantages :

- le code de la boucle principale est plus court (une seulle ligne pour changer la tension appliquée au moteur).  Il est aussi plus explicite (car nous avons donné un nom qui a du sens à notre fonction)
- le code de la fonction contient des tests pour vérifier que la valeur de son paramètre est correcte.  Cela évite évite que le code fasse n'importe quoi.
- la fonction peut être utilisée à plusieurs endroits dans le code (cela est surtout utile dans les codes plus complexes).  El eput aussi être réutilisée dans d'autres programmes.
- si elle est correctement commentée, la fonction peut être utilisée sans avoir besoin de connaitre son fonctionnement interne, ni celui du moteur.  Cela permet à d'autres personnes de l'utiliser plus facilement.  Cela peut aussi permettre de la modifier (pour utiliser une autre moteur/H-bridge par exemple) sans avoir besoin de modifier le code principal.

## Schéma du montage

![Montage de caracMoteur](caracMotSch.png)

L'[*Arduino Nano Every*](https://docs.arduino.cc/hardware/nano-every) est connecté :

- au H-bridge ([*DRI0044*](https://wiki.dfrobot.com/2x1.2A_DC_Motor_Driver__TB6612FNG__SKU__DRI0044)) qui est connecté au moteur DC.  
- à l'encodeur du moteur DC

L'alimentation du circuit est représentée par **VDC1** qui est une source de tension continue.  
Sa tension doit être compatible avec la tension nominale du moteur et, dans tous les cas, inférieure à 12V (qui est la tension max supportée par le *DRI0044*).  
On peut utiliser:

- une alimentation continue
- une batterie

Le *DRI0044* est alimenté par la source (**VDC1**) pour la partie puissance et par l'*Arduino* pour la partie logique (**+5V**).  
L'encodeur est alimenté en 5V par l'Arduino.

L'Arduino doit être connecté, par USB, à un PC qui lira les données envoyées.

## Flowchart

![Flowchart de testEncodeur](caracMotFC.png)

Pour cet exemple, il y a en fait 2 flowcharts : celui du programme principal et celui de l'*ISR*.

L'*ISR* est identique à celle de *testEncodeur*.

Le fonctionnnment du programme principal est le suivant :

- **setup()**
  - initialisation du *hardware* utilisé : **Serial**, les *digital output* connectées au H-bridge, les *digital inputs* connectées à l'encodeur
  - l'*ISR* est associée au changement d'état de **CHA** (un des signaux de l'encodeur)
- **loop()**
  - le code attend que l'utilisateur lui envoye le nombre d'acquisitions désiré
  - il accélère alors le moteur jusqu'à sa vitesse maximum
  - il envoie également, toutes les 10 ms (*PERIOD*), la position mesurée par l'encodeur au PC via **Serial**
  - Lorsque le nombre d'échantillons demandé a été envoyé, il arrête le moteur et retourne au début de la boucle.

## Comportement attendu
