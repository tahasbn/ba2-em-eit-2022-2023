# testMoteur

Code permettant de tester le fonctionnement d'un moteur et de son *H-bridge*.

L'*Arduino* contrôle la vitesse de rotation du moteur DC, en utilisant la [*Pulse Width Modulation*](https://docs.arduino.cc/learn/microcontrollers/analog-output).  
Pour cela, on utilise la fonction [*analogWrite()*](https://www.arduino.cc/reference/en/language/functions/analog-io/analogwrite/) qui permet de fixer le *rapport cyclique* (*duty cycle*) du signal *PWM* envoyé au *H-bridge*.

## Schéma du montage

![Montage de testMoteur](testMotSch.png)

L'[*Arduino Nano Every*](https://docs.arduino.cc/hardware/nano-every) est connecté au H-bridge ([*DRI0044*](https://wiki.dfrobot.com/2x1.2A_DC_Motor_Driver__TB6612FNG__SKU__DRI0044)) qui est connecté au moteur DC.  
L'alimentation du circuit est représentée par **VDC1** qui est une source de tension continue.  
Sa tension doit être compatible avec la tension nominale du moteur et, dans tous les cas, inférieure à 12V (qui est la tension max supportée par le *DRI0044*).  
On peut utiliser:

* une alimentation continue
* une batterie

Le *DRI0044* est alimenté par la source (**VDC1**) pour la partie puissance et par l'*Arduino* pour la partie logique (**+5V**).

## Flowchart

![Flowchart de testMoteur](testMotFC.png)

L'état de la patte **DIR** détermine le sens de rotation du moteur.  
La valeur du *duty cycle* varie entre 0 et 255.

* 0 correspond à une tension nulle appliquée au moteur
* 255 correspond à la tension d'alimentation (**VM**) appliquée au moteur

Le code incrémente le *duty cycle* imposé au moteur de 1 toutes les 5 ms.  
Une fois la tension maximum atteinte, il le décrémente au même rythme.  
Ensuite, il cahne le sens de rotation et recommence.

# Comportement attendu

Le résultat devrait être que la vitesse du moteur varie continuellement de la vitesse maximum dans un sens à la vitesse maximum dans l'autre sens.  
La période complète d'un cycle doit être de $`2 \cdot 256 \cdot 5 ms = 2,56 s`$.
