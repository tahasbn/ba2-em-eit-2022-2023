# Status report

- Groupe : Tuteurs
- Date : 08/06/2022

## Ordre du jour

- [Status report](#status-report)
  - [Ordre du jour](#ordre-du-jour)
  - [Plan d'ensemble](#plan-densemble)
  - [Activités](#activités)
  - [Risques et problèmes](#risques-et-problèmes)
  - [Discussions de contenu](#discussions-de-contenu)

## Plan d'ensemble

![Plan](plans/22-06-08.png)

## Activités 

| Activités terminées la semaine passée | Activités planifiées la semaine prochaine | 
| ------ | ------ |
| Recherche d'idées | Création d'un tremplin |
|  | création d'un prototype de véhicule |
|  | Rédaction d'une note de synthèse sur le projet |

## Risques et problèmes

| Impact \ Probability | Medium     | High  | Issue |
| --                   | --         | --    | --    |
|                      |            |       |       |
| **High**             | Moteurs pas assez puissants pour le saut|       |       |
|                      | Délai de livraison |       |       |
|                      |            |       |       |
| **Medium**           |            |       |       |

## Discussions de contenu
Parmi les idées proposées, plusieurs contenaient un challenge mécanique (vitesse, saut...) pour un véhicule à roues, ce qui n'a pas été fait depuis quelques années.  
Le projet doit aussi être réalisable avec le matériel disponible, pour éviter les problèmes de délais de livraison (suite du COVID).  
Au final, nous avons convergé sur un suivi de ligne avec un saut (grace à un tremplin) au milieu.  
N'ayant jamais fait de sauts dans les projets précédents, il parait prudent de tester sa faisabilité avec les moteurs en stock.  
Joëlle va dimensionner le tremplin.  
Michel va réaliser un prototype de véhicule.  
Laurent va rédiger une note de synthèse décrivangt le projet et contenant le plan du circuit.
